import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TabPage } from './tab.page';

const routes: Routes = [
  {
    path: '',
    component: TabPage,
    children: [
      {
        path: 'home',
        loadChildren: () => import('../views/home/home.module').then( m => m.HomePageModule)
      },
      {
        path: 'musique',
        loadChildren: () => import('../views/music/musique.module').then( m => m.MusiquePageModule)
      },
      {
        path: 'about',
        loadChildren: () => import('../views/about/about.module').then( m => m.AboutPageModule)
      }
    ]
  },
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TabPageRoutingModule {}

