import { RecentplayService } from 'src/app/services/recentplay/recentplay.service';
import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { GenresService } from 'src/app/services/genres/genres.service';
import { App } from '@capacitor/app';
import { IonRouterOutlet , Platform } from '@ionic/angular';
import { Optional } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
resultsOfFilter = []
  slideOpts = {
    initialSlide: 1,
    speed: 400,
    spaceBetween: 100,
  };
  // isLikeIcon = 'heart-outline';
  recentMusicPlay = new Map<any,any>();
  genreOfMusic = []
  genres: any;
  constructor(private router: Router, private genresService: GenresService , private recentPlayService:RecentplayService ,private platform:Platform , @Optional() private routerOutlet?:IonRouterOutlet ) {
    this.recentMusicPlay = this.recentPlayService.showRecentMuscic();
    this.platform.backButton.subscribeWithPriority(-1 , () => {
      if(!this.routerOutlet.canGoBack()){
        App.exitApp();
      }
    });
  }
  ionViewDidEnter() {
    this.genresService.getGenres().subscribe((reponse) => {
      this.genres = reponse;
      this.resultsOfFilter = this.genres;
    })
  }

  handleChange(event) {
    const query = event.target.value.toLowerCase();
    this.resultsOfFilter = this.genres.filter(d => d.name.toLowerCase().indexOf(query) > -1);
    
  }
  toPage(obj) {
    localStorage.setItem("genre", JSON.stringify(obj));
    this.router.navigate(['tab/musique'])
  }
  playMusic(obj1 , obj2){
    localStorage.setItem("music", JSON.stringify(obj1));
    localStorage.setItem("genre", JSON.stringify(obj2));
    this.router.navigate(['playmusic'])
  }
}
