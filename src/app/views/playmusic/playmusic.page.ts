import { GenresService } from 'src/app/services/genres/genres.service';
import { NativeAudio } from '@capgo/native-audio';
import { LecteurService } from './../../services/lecteur/lecteur.service';
import { Component, OnInit } from '@angular/core';
import { RecentplayService } from 'src/app/services/recentplay/recentplay.service';
import { Router } from '@angular/router';
import { Filesystem, Directory, Encoding, ReaddirOptions } from '@capacitor/filesystem';

@Component({
  selector: 'app-playmusic',
  templateUrl: './playmusic.page.html',
  styleUrls: ['./playmusic.page.scss'],
})
export class PlaymusicPage implements OnInit {
  playIcon = 'pause';
  audioDuration: String;
  indexOfCurrentMusic: number;
  currentPosition: number;
  restTime: string;
  musicSelected: any;
  genreSelected: any;
  duration: number;
  currentTime: number;
  curentPosition: String;
  currentPositionVolume: number;
  actualVolume = 0.5;
  isPlaying: any;
  recentMusicPlay = [];
  allMusicOfGenre = [];
  musicOfGenre = []
  listMusics = []
  test: any;

  constructor(private router: Router, private lecteurService: LecteurService, private recentPlayService: RecentplayService, private genresService: GenresService) {
    setInterval(() => {
      this.setVolume();
      this.getCurrentPosition();
      this.currentPositionVolume = this.actualVolume;
      this.currentPosition = this.currentTime;
      this.restTime = this.convertSec(this.currentPosition)
      if (this.currentTime >= this.duration)
        this.playIcon = 'play'
      // alert(this.currentTime)
      // alert(this.duration)
    }, 1000);
    if (this.currentTime >= this.duration)
      this.playIcon = 'play'
    // this.recentMusicPlay = this.recentPlayService.showRecentMuscic();

  }
  takeNewPosition() {
    NativeAudio.play({
      assetId: this.test.name,
      time: this.currentPosition
    })
  }

  ngOnInit() {

  }

  convertSec(secondes: number) {
    const min = Math.floor((secondes / 60 / 60) * 60);
    const sec = Math.floor(((secondes / 60 / 60) * 60 - min) * 60);
    this.restTime = min + 'm ' + sec + 's';
    return this.restTime;
  }
  // changePosition() {
  //   console.log('changePosition: ' + this.currentPosition);
  //   this.file.seekTo(this.currentPosition*1000);
  //   this.convertSec(this.audioDuration - this.currentPosition);
  // }
  musics: any;
  ionViewDidEnter() {

    const retrieve = localStorage.getItem("music");
    const retrieve2 = localStorage.getItem("genre");
    this.musicSelected = JSON.parse(retrieve);
    this.genreSelected = JSON.parse(retrieve2);
    // alert(this.musicSelected.name)
    // alert(this.genreSelected.name)
    // this.allMusicOfGenre = this.genresService.showMusics();

    const options: ReaddirOptions = {
      path: `musics/${this.genreSelected.repository}`,
      directory: Directory.ExternalStorage
      // directory:Directory.Data
      //creer le dossier music dans files
    };
    Filesystem.readdir(options).then((reponse => {
      this.musicOfGenre = reponse.files;
      //alert(this.musicOfGenre.length)
      this.listMusics = this.musicOfGenre
      var test = []
      for (let i = 0; i < this.musicOfGenre.length; i++) {
        test = this.musicOfGenre[i].name.split(".");
        this.listMusics[i].name = test[0];
      }
      this.genresService.musicOfGenre(this.listMusics);
      this.allMusicOfGenre = this.genresService.showMusics();
      for (let i = 0; i < this.allMusicOfGenre.length; i++) {
        if (this.allMusicOfGenre[i].name == this.musicSelected.name) {
          this.indexOfCurrentMusic = i;
        }
      }
      this.test = this.allMusicOfGenre[this.indexOfCurrentMusic]
      this.recentPlayService.addToRecent(this.musicSelected, this.genreSelected)
      this.lecteurService.loadSong(this.allMusicOfGenre[this.indexOfCurrentMusic]).then(data => {
        this.lecteurService.playSong(this.allMusicOfGenre[this.indexOfCurrentMusic], this.currentTime);
        this.getCurrentPosition();
        this.getDuration();
      });
      if (this.currentTime >= this.duration) {
        this.playIcon = 'pause';
      }
    }));

  }
  getDuration() {
    NativeAudio.getDuration({
      assetId: this.test.name,
    }).then(result => {
      this.duration = result.duration;
      this.audioDuration = this.convertSec(result.duration);
    })
  }
  playNext() {
    this.currentTime = 0.0
    this.playIcon = 'pause'
    this.actualVolume = 0.5
    if (this.indexOfCurrentMusic < this.allMusicOfGenre.length - 1) {
      this.lecteurService.unloadSong(this.allMusicOfGenre[this.indexOfCurrentMusic]);
      this.indexOfCurrentMusic = this.indexOfCurrentMusic + 1;
      this.lecteurService.loadSong(this.allMusicOfGenre[this.indexOfCurrentMusic]).then(data => {
        this.lecteurService.playSong(this.allMusicOfGenre[this.indexOfCurrentMusic], this.currentTime);
        this.getCurrentPosition();
        // this.getCurrentPositionVolume();
        this.getDuration();
      });
    }
    else {
      this.indexOfCurrentMusic = 0;
      this.lecteurService.unloadSong(this.allMusicOfGenre[this.indexOfCurrentMusic]);
      // this.indexOfCurrentMusic = this.indexOfCurrentMusic + 1;
      this.lecteurService.loadSong(this.allMusicOfGenre[this.indexOfCurrentMusic]).then(data => {
        this.lecteurService.playSong(this.allMusicOfGenre[this.indexOfCurrentMusic], this.currentTime);
        this.getCurrentPosition();
        // this.getCurrentPositionVolume();
        this.getDuration();
      });
    }

    this.test = this.allMusicOfGenre[this.indexOfCurrentMusic]
  }
  playPrevious() {
    this.currentTime = 0.0
    this.playIcon = 'pause'
    this.actualVolume = 0.5
    if (this.indexOfCurrentMusic > 0) {
      this.lecteurService.unloadSong(this.allMusicOfGenre[this.indexOfCurrentMusic]);

      this.indexOfCurrentMusic = this.indexOfCurrentMusic - 1;
      this.lecteurService.loadSong(this.allMusicOfGenre[this.indexOfCurrentMusic]).then(data => {
        this.lecteurService.playSong(this.allMusicOfGenre[this.indexOfCurrentMusic], this.currentTime);
        this.getCurrentPosition();
        // this.getCurrentPositionVolume();
        this.getDuration();
      });
    }
    else {
      this.indexOfCurrentMusic = this.allMusicOfGenre.length - 1;
      this.lecteurService.unloadSong(this.allMusicOfGenre[this.indexOfCurrentMusic]);
      this.lecteurService.loadSong(this.allMusicOfGenre[this.indexOfCurrentMusic]).then(data => {
        this.lecteurService.playSong(this.allMusicOfGenre[this.indexOfCurrentMusic], this.currentTime);
        this.getCurrentPosition();
        // this.getCurrentPositionVolume();
        this.getDuration();
      });
    }
    this.test = this.allMusicOfGenre[this.indexOfCurrentMusic]
  }
  getCurrentPosition() {
    NativeAudio.getCurrentTime({
      assetId: this.test.name
    }).then(result => {
      this.currentTime = result.currentTime;
      this.curentPosition = this.convertSec(this.currentTime)
    })
  }
  handleSong() {
    if (this.currentTime >= this.duration) {
      this.currentTime = 0.0
    }
    if (this.playIcon == 'play') {
      this.lecteurService.playSong(this.allMusicOfGenre[this.indexOfCurrentMusic], this.currentTime);
      this.playIcon = 'pause';
    }
    else {
      this.playIcon = 'play';
      this.lecteurService.pauseSong(this.test);
    }
  }
  setVolume() {
    NativeAudio.setVolume({
      assetId: this.test.name,
      volume: this.actualVolume
    })
  }
  addVolume() {
    if (this.actualVolume < 1)
      this.actualVolume = this.actualVolume + 0.1;
    // alert(this.actualVolume)
  }
  reduceVolume() {
    if (this.actualVolume > 0)
      this.actualVolume = this.actualVolume - 0.1;
  }
}


