import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { PlaymusicPage } from './playmusic.page';

describe('PlaymusiquePage', () => {
  let component: PlaymusicPage;
  let fixture: ComponentFixture<PlaymusicPage>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ PlaymusicPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(PlaymusicPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
// NativeAudio.preload({
//   assetId: "Meiitod",
//   assetPath: "Meiitod.mp3",
//   audioChannelNum: 1,
//   isUrl: false
// });


// NativeAudio.play({
//   assetId: "Meiitod",
//   time: 6.0
// });

// NativeAudio.loop({
//   assetId: 'Meiitod',
// });

// NativeAudio.stop({
//   assetId: 'Meiitod',
// });

// NativeAudio.unload({
//   assetId: 'Meiitod',
// });

// NativeAudio.setVolume({
//   assetId: 'Meiitod',
//   volume: 0.4,
// });

// NativeAudio.getDuration({
//   assetId: 'Meiitod'
// })
// .then(result => {
//   console.log(result.duration);
// })

// /**
//  * this method will get the current time of a playing audio file.
//  * only works if channels == 1
//  */
// NativeAudio.getCurrentTime({
//   assetId: 'Meiitod'
// })
// .then(result => {
//   console.log(result.currentTime);
// })

// /**
//  * This method will return false if audio is paused or not loaded.
//  * @param assetId - identifier of the asset
//  * @returns {isPlaying: boolean}
//  */
// NativeAudio.isPlaying({
//   assetId: 'Meiitod'
// })
// .then(result => {
//   console.log(result.isPlaying);
// })
// const writeSecretFile = async () => {
//   await Filesystem.writeFile({
//     path: 'secrets/text.txt',
//     data: "This is a test",
//     directory: Directory.Documents,
//     encoding: Encoding.UTF8,
//   });
// };

// const readSecretFile = async () => {
//   const contents = await Filesystem.readFile({
//     path: 'secrets/text.txt',
//     directory: Directory.Documents,
//     encoding: Encoding.UTF8,
//   });

//   console.log('secrets:', contents);
// };

// const deleteSecretFile = async () => {
//   await Filesystem.deleteFile({
//     path: 'secrets/text.txt',
//     directory: Directory.Documents,
//   });
// };

// const readFilePath = async () => {
//   // Here's an example of reading a file with a full file path. Use this to
//   // read binary data (base64 encoded) from plugins that return File URIs, such as
//   // the Camera.
//   const contents = await Filesystem.readFile({
//     path: 'file:///var/mobile/Containers/Data/Application/22A433FD-D82D-4989-8BE6-9FC49DEA20BB/Documents/text.txt'
//   });

//   console.log('data:', contents);
// };


