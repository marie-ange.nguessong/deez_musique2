import { LecteurService } from 'src/app/services/lecteur/lecteur.service';
import { GenresService } from 'src/app/services/genres/genres.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Filesystem, Directory, Encoding, ReaddirOptions } from '@capacitor/filesystem';



@Component({
  selector: 'app-musique',
  templateUrl: './musique.page.html',
  styleUrls: ['./musique.page.scss'],
})
export class MusiquePage implements OnInit {
  genreSelected: any;
  musicOfGenres = [];
  musics = [];
  listOfMusics = []
  resultsOfFilter = [];
  query: any
  isLikeIcon = 'heart-outline';

  constructor(private router: Router , private genresService:GenresService) {
  }
  ionViewDidEnter() {
    const retrieve = localStorage.getItem("genre");
    this.genreSelected = JSON.parse(retrieve);
    const options: ReaddirOptions = {
      path: `musics/${this.genreSelected.repository}`,
      directory: Directory.ExternalStorage
    };
    Filesystem.readdir(options).then((reponse => {
      this.listOfMusics = reponse.files;
      this.resultsOfFilter = this.listOfMusics;
      
      var test = []
      for (let i = 0; i < this.resultsOfFilter.length; i++) {
        test = this.resultsOfFilter[i].name.split(".");
        this.resultsOfFilter[i].name = test[0];
        Object.defineProperty(this.resultsOfFilter[i], 'isLike', {
          value: this.isLikeIcon,
          writable: true
        });
      }
    }))


  }
  ngOnInit() {

  }
  liked(i){
    if(this.isLikeIcon == 'heart-outline'){
      this.isLikeIcon = 'heart-sharp'
    }  
    else
      this.isLikeIcon = 'heart-outline'
    this.resultsOfFilter[i].isLike = this.isLikeIcon
  }
  toPage(obj) {
    localStorage.setItem("music", JSON.stringify(obj));
    localStorage.setItem("genre", JSON.stringify(this.genreSelected));
    this.router.navigate(['playmusic'])
  }
  handleChange(event) {
    this.query = event.target.value.toLowerCase();
    this.resultsOfFilter = this.listOfMusics.filter(d => d.name.toLowerCase().indexOf(this.query) > -1);

  }
}
