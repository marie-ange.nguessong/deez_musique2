import { TestBed } from '@angular/core/testing';

import { RecentplayService } from './recentplay.service';

describe('RecentplayService', () => {
  let service: RecentplayService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(RecentplayService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
