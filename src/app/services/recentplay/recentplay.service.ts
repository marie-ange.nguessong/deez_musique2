import { Injectable } from '@angular/core';


@Injectable({
  providedIn: 'root'
})
export class RecentplayService {
  musicWithGenre = new Map<any,any>();
  cash = []
  constructor() { }
  addToRecent(song , genre){
    if(this.cash.indexOf(song.name) < 0){
      this.musicWithGenre.set(song , genre);
      this.cash.push(song.name)
    }
  }

  showRecentMuscic(){
    return this.musicWithGenre;
  }

 
}
