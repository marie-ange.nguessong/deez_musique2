import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class PlaylistService {
  playlist = new Map<any , any>();
  constructor() { }

  addPlaylist(obj:any){
    this.playlist.set(obj.id , obj.name)
  }

  deletePlaylist(obj:any){
    this.playlist.delete(obj)
  }
  showPlaylist(){
    return this.playlist;
  }
}
