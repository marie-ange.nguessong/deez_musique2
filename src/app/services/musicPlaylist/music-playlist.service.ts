import { importProvidersFrom, Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class MusicPlaylistService {
  musicPlayl = new Map()
  constructor() { }

  addMusicToPlaylist(idM:any, idP:any){
    // this.musicPlayl.push(idM , idP)
    this.musicPlayl.set(idM , idP)
  }

  showMusicOfPlaylist(idp:any){
    let musicOfP : any;
    this.musicPlayl.forEach((music)=>{
      musicOfP = music?music.idP ==idp : ''
    })
    return musicOfP
    // this.musicPlayl = this.musicPlayl.filter((music:any)=>{
    //   return music.idP == idp;
    // })
  }
  deleteMusicFromPlaylist(idM:any){
    this.musicPlayl.delete(idM)
  }

}
