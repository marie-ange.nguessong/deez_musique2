import { Injectable } from '@angular/core';
import { NativeAudio } from '@capgo/native-audio';

@Injectable({
  providedIn: 'root'
})
export class LecteurService {
  isPlaying = ""


  constructor() { }
  loadSong(song) {
    return NativeAudio.preload({
      assetId: song.name,
      assetPath: song.uri,
      audioChannelNum: 1,
      isUrl: true

    });
  }
  playSong(song, time) {
    if (this.isPlaying !== "" && this.isPlaying !== song.name) {
      NativeAudio.unload({
        assetId: this.isPlaying
      });
      this.isPlaying = song.name;
      return NativeAudio.play({
        assetId: song.name,
        time: time
      });
    }
    else {
      this.isPlaying = song.name;
      return NativeAudio.play({
        assetId: song.name,
        time: time
      });
    }

  }
  pauseSong(song) {
    return NativeAudio.pause({
      assetId: song.name
    });
  }
  isMusicPlaying(song) {
    return NativeAudio.isPlaying({
      assetId: 'song.name'
    })
  }
  unloadSong(song) {
    return NativeAudio.unload({
      assetId: song.name,
    });
  }

}
