import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
@Injectable({
  providedIn: 'root'
})
export class GenresService {
  listOfMusics = []
  constructor(private _http: HttpClient) { }
  getGenres(){
    return this._http.get('assets/genres.json');
  }
  musicOfGenre(musics){
    this.listOfMusics = musics;
  }
  showMusics(){
    return this.listOfMusics;
  }
  test(musics){
    this.listOfMusics = musics;
    return this.listOfMusics;
  }
}
