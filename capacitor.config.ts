import { CapacitorConfig } from '@capacitor/cli';

const config: CapacitorConfig = {
  appId: 'org.isj.isi.richemusic',
  appName: 'RicheMusic',
  webDir: 'www',
  bundledWebRuntime: false
};

export default config;
